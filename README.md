# Seamless Protocol-Independent IoT Interoperability
This repository the draft version of the ACM HotNets '21 paper "Do we want the New Old Internet?: Towards Seamless and Protocol-Independent IoT Application Interoperability". This version was extended in summer 2023 and updated with new results. 
